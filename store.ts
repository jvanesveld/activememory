/**
 * Super simple redux like store
 */
export class Store<T> {
  callbacks: ((old_state: T, new_state: T) => void)[] = [];
  state: T;
  old_state: T;
  manual: boolean;

  constructor(initState: T, manual = false) {
    this.state = initState;
    this.old_state = initState;
    this.manual = manual;
  }

  setState(mutator: (state: T) => T) {
    this.state = mutator(this.state);
    if (!this.manual) {
      this.update();
    }
  }

  listen(callback: (oldState: T, newState: T) => void) {
    this.callbacks.push(callback);
    return () => {
      const index = this.callbacks.indexOf(callback);
      this.callbacks.splice(index, 1);
    };
  }

  update() {
    this.callbacks.forEach((callback) => callback(this.old_state, this.state));
    this.old_state = this.state;
  }
}
