# Active Memory

Simple redux like store that saves changes to a JSON file

## Example
Code
```
import { loadState } from "./mod.ts";

const memory = await loadState("example.json", { test: 0 });

memory.listen(({ test: oldTest }, { test: newTest }) => {
  console.log(`Test was changed from ${oldTest} to ${newTest}`);
});

memory.setState((oldState) => ({
  ...oldState,
  test: 1,
}));
```
Output
```
Test was changed from 0 to 1
```
`example.json`
```
{"test":1}
```