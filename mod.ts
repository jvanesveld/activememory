export * from "./store.ts";
import { Store } from "./store.ts";

/**
 * Loads and saves data using local storage
 * @param name Name of the field to load
 * @param initialState The state to use if no file was found
 * @returns Store that saves changes to local storage
 */
export function storeFromStorage<T>(name: string, initialState: T) {
  const state = { ...initialState };
  const storage = localStorage.getItem(name);
  if (storage) {
    Object.assign(state, JSON.parse(storage));
  }
  localStorage.setItem(name, JSON.stringify(state));
  
  const store = new Store<T>(state);
  store.listen((_, newState) => {
    localStorage.setItem(name, JSON.stringify(newState));
  });
  return store;
}

/**
 * Loads and saves data using a JSON file
 * @param path Path to JSON file to load
 * @param initialState The state to use if no file was found
 * @returns Store that saves changes to the specified path
 */
export async function storeFromFile<T>(path: string, initialState: T) {
  const state = { ...initialState };
  try {
    await Deno.stat(path);
    Object.assign(state, JSON.parse(await Deno.readTextFile(path)));
  } catch (error) {
    if (error instanceof Deno.errors.NotFound) {
      await Deno.writeTextFile(path, JSON.stringify(state));
    } else {
      throw error;
    }
  }
  const store = new Store<T>(state);
  store.listen(async (_, newState) => {
    await Deno.writeTextFile(path, JSON.stringify(newState));
  });
  return store;
}
